<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        $users = [
            [
                'name' => 'Test User',
                'email' => 'test_1@email.com',
                'password' => bcrypt('111111'),
                'api_key' => 'h37jweg#h8%_hJhQ098lbcyekX9e',
                'created_at' => now(),
            ],[
                'name' => 'Shazzad Hossain Kanon',
                'email' => 'test_2@email.com',
                'password' => bcrypt('111111'),
                'api_key' => 'ue9#l0978h8%_hJhQ098lbcyeUjV9G',
                'created_at' => now(),
            ],[
                'name' => 'HARAPROSAD BISWAS',
                'email' => 'test_3@email.com',
                'password' => bcrypt('111111'),
                'api_key' => 'ZopR65Dcb#h8%_hJhQ058OpcyekX5p',
                'created_at' => now(),
            ],[
                'name' => 'MD.IMRAN KHAN',
                'email' => 'test_4@email.com',
                'password' => bcrypt('111111'),
                'api_key' => 'lKmn45Dweg#$gh8%_hJhQ098l45poRt',
                'created_at' => now(),
            ],[
                'name' => 'M SHAKHAWAT HOSSAIN',
                'email' => 'test_5@email.com',
                'password' => bcrypt('111111'),
                'api_key' => 'LjSw98*#h8%_hJhQ098lbcyeYu4fS',
                'created_at' => now(),
            ],

        ];

        DB::table('users')->insert($users);
    }
}
