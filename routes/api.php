<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'middleware' => 'api', 'namespace' => 'App\Http\Controllers\Api'], function () {
    Route::group([
        'middleware' => 'ApiAuth',
        'prefix' => 'exam'
    ], function ($router) {
        Route::apiResource('posts', 'PostsController');
        Route::post('posts/{id}/comments', 'PostsController@storeComment');
    });
});
