## Installation

```sh
cd project_directory
composer install
```

* copy .env.example to .env,
* Edit .env, update DB_DATABASE, DB_USERNAME, DB_PASSWORD
* ```php artisan key:generate```
* ```php artisan migrate --seed```
* That's it

## Server Requirements
* PHP >= 7.3
* BCMath PHP Extension
* Ctype PHP Extension
* Fileinfo PHP extension
* JSON PHP Extension
* Mbstring PHP Extension
* OpenSSL PHP Extension
* PDO PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
