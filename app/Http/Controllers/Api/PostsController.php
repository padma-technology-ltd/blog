<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Exceptions\PostErrorException;
use App\Http\Requests\CommentCreateRequest;
use App\Http\Requests\PostCreateRequest;
use App\Http\Requests\PostEditRequest;
use App\Http\Resources\CommentResource;
use App\Http\Resources\PostDetailResource;
use App\Http\Resources\PostResource;
use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    /**
     * @var Post
     */
    private $postModel;

    /**
     * @var Comment
     */
    private $commentModel;

    /**
     * PostsController constructor.
     * @param Post $post
     * @param Comment $comment
     */
    public function __construct(Post $post, Comment $comment)
    {
        $this->postModel = $post;
        $this->commentModel = $comment;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws PostErrorException
     */
    public function index(Request $request)
    {
        try {
            $posts = $this->postModel->where('created_by', $request->auth_user->id)
                                    ->orderBy('id', 'desc')
                                    ->limit(30)
                                    ->get();
            return PostResource::collection($posts);
        } catch (\ErrorException $exception) {
            throw new PostErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param PostCreateRequest $request
     * @return PostResource
     * @throws PostErrorException
     */
    public function store(PostCreateRequest $request)
    {
        try {
            $this->postModel->title = $request->title;
            $this->postModel->description = $request->description;
            $this->postModel->created_by = $request->auth_user->id;
            $this->postModel->save();
            return new PostResource($this->postModel);
        } catch (\ErrorException $exception) {
            throw new PostErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param PostEditRequest $request
     * @param $id
     * @return PostResource
     * @throws PostErrorException
     */
    public function update(PostEditRequest $request, $id)
    {
        try {
            $this->postModel = $this->postModel->where('id', $id)
                                                ->where('created_by', $request->auth_user->id)
                                                ->firstOrFail();

            $this->postModel->title = $request->title;
            $this->postModel->description = $request->description;
            $this->postModel->modified_by = $request->auth_user->id;
            $this->postModel->save();
            return new PostResource($this->postModel);
        } catch (ModelNotFoundException $exception) {
            throw new PostErrorException('Post not found', 400);
        } catch (\ErrorException $exception) {
            throw new PostErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return PostDetailResource
     * @throws PostErrorException
     */
    public function show(Request $request, $id)
    {
        try {
            $post = $this->postModel->where('id', $id)
                ->where('created_by', $request->auth_user->id)
                ->firstOrFail();

            return new PostDetailResource($post);
        } catch (ModelNotFoundException $exception) {
            throw new PostErrorException('Post not found', 400);
        } catch (\ErrorException $exception) {
            throw new PostErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws PostErrorException
     */
    public function destroy(Request $request, $id)
    {
        try {
            $post = $this->postModel->where('id', $id)
                                    ->where('created_by', $request->auth_user->id)
                                    ->firstOrFail();

            $post->delete();

            return response()->json(
                ["message" => 'Post successfully deleted'],
                200
            );
        } catch (ModelNotFoundException $exception) {
            throw new PostErrorException('Post not found', 400);
        } catch (\ErrorException $exception) {
            throw new PostErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param CommentCreateRequest $request
     * @param $id
     * @return CommentResource
     * @throws PostErrorException
     */
    public function storeComment(CommentCreateRequest $request, $id)
    {
        try {
            $post = $this->postModel->findOrFail($id);

            $this->commentModel->post_id = $post->id;
            $this->commentModel->message = $request->message;
            $this->commentModel->created_by = $request->auth_user->id;
            $this->commentModel->save();
            return new CommentResource($this->commentModel);
        } catch (ModelNotFoundException $exception) {
            throw new PostErrorException('Post not found', 400);
        } catch (\ErrorException $exception) {
            throw new PostErrorException($exception->getMessage(), 500);
        }
    }
}
