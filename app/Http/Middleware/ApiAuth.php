<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->header('APIKEY') && $user = User::where('api_key', $request->header('APIKEY'))->first()) {
            $request->request->add(['auth_user' => $user]);
            return $next($request);
        }

        throw new UnauthorizedException("Authentication failed", 401);
    }
}
