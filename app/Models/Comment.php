<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function created_user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
